import java.util.Random;
import java.util.Scanner;

public class miniGame {
    static int maxRand = 100;
    static Random rand = new Random();
    static Scanner inp = new Scanner(System.in);
    private String nama;
    private int score,kesempatan=3;

    public miniGame(String nama){
        this.nama = nama;
    }

    // =>>>>> MAIN METHOD <<<<<=
    void Game(){
        int choose, choose2, next=1;
        // memilih mini game
        System.out.println("\n===================");
        System.out.println("Selamat Datang " + getNama() + " !!");
        System.out.println("1. Menghitung Bangun Datar");
        System.out.println("2. Menghitung Bangun Ruang");
        System.out.println("===================");
        System.out.println("Pilih Mini Game:");
        // input bangun yang dipilih
        choose = Integer.parseInt(inp.nextLine());

        // Switch Case untuk memilih bangun
        switch (choose){
            case 1:
                while(next==1) {
                    double a = rand.nextInt(maxRand), b = rand.nextInt(maxRand), c = rand.nextInt(maxRand);
                    int chsKL = rand.nextInt(2);

                    String[] bangunD = {"Persegi", "Persegi Panjang", "Lingkaran", "Segitiga"};
                    showPilihBangun(bangunD, "datar");
                    //input
                    choose2 = Integer.parseInt(inp.nextLine());
                    do{
                        next = QuizD(choose2, a, b, c, chsKL);
                        if(next==0){
                            kesempatan -= 1;
                            if (kesempatan == 0){
                                System.out.println("\n=====Game Over=====");
                            } else {
                                System.out.println("\nKamu masih memiliki " + kesempatan + " kesempatan");
                            }
                        }

                    }while (kesempatan!=0 && next==0);
                }
                gameOver();

                break;
            case 2:
                // String[] bangunR = {"Kubus", "Balok", "Tabung"};
                // showPilihBangun(bangunR, "ruang");
                //input
                // choose2 = Integer.parseInt(inp.nextLine());

                System.out.println("Coming Soon!!");
                break;
            case 3:
                System.out.println("High Score:");

            default:
                System.out.println("Pilihan tidak sesuai!");
        }
    }

    // =>>>>> SUB METHOD <<<<<=
    void showPilihBangun(String[] nama, String bangun){
        System.out.println("\n===================");
        for(int i=0; i<nama.length; i++){
            System.out.println((i+1) + ". " + nama[i]);
        }
        System.out.println("===================");
        System.out.println("Pilih bangun " + bangun + " yang akan dihitung :");
    }

    // ==== Methode memberikan soal quiz ====
    int QuizD(int choosen, double a, double b, double c, int chsKL){
        int next = 0;

        switch (choosen){
            case 1: // <== PERSEGI ==>
                persegi Persegi = new persegi();
                Persegi.sisi = a;

                if(chsKL == 0){
                    // KELILING
                    double correctAnswer = Persegi.keliling();

                    System.out.println("Hitung Keliling Persegi berikut: ");
                    System.out.println("Sisi : " + Persegi.sisi);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                } else {
                    // LUAS
                    double correctAnswer = Persegi.luas();

                    System.out.println("Hitung Luas Persegi berikut: ");
                    System.out.println("Sisi : " + Persegi.sisi);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                }
                break;

            case 2: // <== PERSEGI PANJANG ==>
                persegiPanjang PersegiPanjang = new persegiPanjang();
                PersegiPanjang.panjang = a;
                PersegiPanjang.tinggi = b;

                if(chsKL == 0){
                    // KELILING
                    double correctAnswer = PersegiPanjang.keliling();

                    System.out.println("Hitung Keliling Persegi berikut: ");
                    System.out.println("Panjang : " + PersegiPanjang.panjang);
                    System.out.println("Tinggi : " + PersegiPanjang.tinggi);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                } else {
                    // LUAS
                    double correctAnswer = PersegiPanjang.luas();

                    System.out.println("Hitung Luas Persegi berikut: ");
                    System.out.println("Panjang : " + PersegiPanjang.panjang);
                    System.out.println("Tinggi : " + PersegiPanjang.tinggi);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                }
                break;
            case 3:// <== LINGKARAN ==>
                lingkaran Lingkaran = new lingkaran();
                Lingkaran.r = a;

                if(chsKL == 0){
                    // KELILING
                    double correctAnswer = Lingkaran.keliling();

                    System.out.println("Hitung Keliling Lingkaran berikut: ");
                    System.out.println("Jari-jari : " + Lingkaran.r);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                } else {
                    // LUAS
                    double correctAnswer = Lingkaran.luas();

                    System.out.println("Hitung Luas Persegi berikut: ");
                    System.out.println("Jari-jari : " + Lingkaran.r);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                }
                break;
            case 4:// <== SEGITIGA ==>
                segitiga Segitiga = new segitiga();
                Segitiga.a = a;
                Segitiga.alas = a;
                Segitiga.b = b;
                Segitiga.tinggi = b;
                Segitiga.c = c;

                if(chsKL == 0){
                    // KELILING
                    double correctAnswer = Segitiga.keliling();

                    System.out.println("Hitung Keliling Segitiga berikut: ");
                    System.out.println("Sisi A : " + Segitiga.a);
                    System.out.println("Sisi B : " + Segitiga.b);
                    System.out.println("Sisi C : " + Segitiga.c);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                } else {
                    // LUAS
                    double correctAnswer = Segitiga.luas();

                    System.out.println("Hitung Luas Segitiga berikut: ");
                    System.out.println("Alas : " + Segitiga.alas);
                    System.out.println("Tinggi : " + Segitiga.tinggi);
                    System.out.println("Jawab : ");
                    double answer = Double.parseDouble(inp.nextLine());

                    next = isCorrect(answer, correctAnswer);
                }
                break;
            default:
                System.out.println("Pilihanmu tidak sesuai");
                break;
        }
        return next;
    }

    // ==== Methode Mengecek Jawaban ====
    int isCorrect(double answ, double Cansw){
        if(answ == Cansw){
            addScore();
            System.out.println("Jawabanmu Benar");
            return 1;
        }else{
            System.out.println("Jawabanmu Salah");
            return 0;
        }
    }

    void gameOver(){
        System.out.println("Nama : " + this.nama);
        System.out.println("Score : " + this.score);
        System.out.println("===================");
    }

    // =>>>>> MODIFIER <<<<<=
    public String getNama(){
        return this.nama;
    }
    public int getScore(){
        return this.score;
    }

    public void addScore(){
        this.score += 100;
    }
}
