public class bangunRuang {

    double volum(){
        return 0;
    }

    double luasPermukaan(){
        return 0;
    }

    void showHasil(){
        double vol = volum(), luP = luasPermukaan();
        System.out.println("\n===================");
        System.out.println("Volum : " + vol);
        System.out.println("Luas Permukaan : " + luP);
        System.out.println("===================");

    }
}

class kubus extends bangunRuang {
    double sisi;

    @Override
    double volum(){
        double vol = 3 * sisi;
        return vol;
    }

    @Override
    double luasPermukaan(){
        double luasP = 6 * (sisi*sisi);
        return luasP;
    }
}

class balok extends bangunRuang{
    double tinggi, panjang, lebar;

    @Override
    double volum(){
        double vol = panjang * tinggi * lebar;
        return vol;
    }

    @Override
    double luasPermukaan(){
        double luasP = 2 * (panjang*lebar + panjang*tinggi + lebar*tinggi );
        return luasP;
    }
}

class tabung extends bangunRuang{
    double tinggi, jari;

    @Override
    double volum(){
        double vol = 3.14 * (jari*jari) * tinggi;
        return vol;
    }

    @Override
    double luasPermukaan(){
        double luasP = 2 * 3.14 * jari * (jari + tinggi);
        return luasP;
    }
}
