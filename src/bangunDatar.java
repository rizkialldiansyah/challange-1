public class bangunDatar {
    double luas(){
        return 0;
    }

    double keliling(){
        return 0;
    }

    void showHasil(){
        double kel = keliling(), lu = luas();
        System.out.println("\n===================");
        System.out.println("Keliling : " + kel);
        System.out.println("Luas : " + lu);
        System.out.println("===================");

    }

}

class persegi extends bangunDatar {
    double sisi;

    @Override
    double luas(){
        double lu = sisi*sisi;
        return lu;
    }

    @Override
    double keliling(){
        double kel = 4*sisi;
        return kel;
    }
}

class persegiPanjang extends bangunDatar {
    double tinggi, panjang;

    @Override
    double luas(){
        double lu = tinggi*panjang;
        return lu;
    }

    @Override
    double keliling(){
        double kel = (2*tinggi) + (2*panjang);
        return kel;
    }
}

class lingkaran extends bangunDatar {
    double r;

    @Override
    double luas(){
        double lu = 3.14*(r*r);
        return lu;
    }

    @Override
    double keliling(){
        double kel = 2 * 3.14 * r;
        return kel;
    }
}

class segitiga extends bangunDatar {
    double a, b, c, tinggi, alas;


    @Override
    double luas(){

        double lu = 0.5 * alas * tinggi;
        return lu;
    }

    @Override
    double keliling(){
        double kel = a + b + c;
        return kel;
    }
}
