import java.lang.annotation.Target;
import java.util.Scanner;

public class main {
    static Scanner inp = new Scanner(System.in);

    public static void main(String[] args) {

            int choose, choose2, exit=0;
            String goStart;
            while(exit == 0){
                System.out.println("=== CALLCULATOR ===");
                System.out.println("1. Menghitung Bangun Datar");
                System.out.println("2. Menghitung Bangun Ruang");
                System.out.println("3. Mini Game");
                System.out.println("0. Keluar");
                System.out.println("===================");
                System.out.println("Pilih menu: ");
                choose = Integer.parseInt(inp.nextLine());

                switch (choose){
                    case 1: // Menghitung Bangun Datar
                        String[] bangunD = {"Persegi", "Persegi Panjang", "Lingkaran", "Segitiga"};
                        showPilihBangun(bangunD, "datar");
                        //input
                        choose2 = Integer.parseInt(inp.nextLine());
                        hitungD(choose2);
                        // Back to start
                        System.out.println("Klik ENTER, untuk kembali ke menu awal");
                        goStart = inp.nextLine();
                        break;
                    case 2: // Menghitung Bangun Ruang
                        String[] bangunR = {"Kubus", "Balok", "Tabung"};
                        showPilihBangun(bangunR, "ruang");
                        //input
                        choose2 = Integer.parseInt(inp.nextLine());
                        hitungR(choose2);
                        // Back to start
                        System.out.println("Klik ENTER, untuk kembali ke menu awal");
                        goStart = inp.nextLine();
                        break;
                    case 3: // Mini Game
                        System.out.println("\n===================");
                        System.out.println("Masukan Nama: ");
                        String Nama = inp.nextLine();
                        System.out.println("===================");
                        miniGame player = new miniGame(Nama);
                        player.Game();
                        // Back to start
                        System.out.println("Klik ENTER, untuk kembali ke menu awal");
                        goStart = inp.nextLine();
                        break;
                    case 0:
                        exit = 1;
                        System.out.println("\n===================");
                        System.out.println("Terimakasih!");
                        System.out.println("===================");
                        break;
                    default:
                        break;
                }
            }

        }
    // =>>>>> SUB METHOD <<<<<=

    // ___ Menampilkan Pilihan Bangun ___
    static void showPilihBangun(String[] nama, String bangun){
        System.out.println("\n===================");
        for(int i=0; i<nama.length; i++){
            System.out.println((i+1) + ". " + nama[i]);
        }
        System.out.println("===================");
        System.out.println("Pilih bangun " + bangun + " yang akan dihitung :");
    }

    // ___ Menghitung Bangun Datar ___
    static void hitungD(int choose){
        switch (choose){
            case 1: // persegi
                persegi Persegi = new persegi();

                System.out.println("Masukan Sisi : ");
                Persegi.sisi = Integer.parseInt(inp.nextLine());

                Persegi.showHasil();
                break;
            case 2:
                persegiPanjang PersegiP = new persegiPanjang();

                System.out.println("Masukan Tinggi : ");
                PersegiP.tinggi = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Panjang");
                PersegiP.panjang = Integer.parseInt(inp.nextLine());

                PersegiP.showHasil();
                break;
            case 3:
                lingkaran Lingkaran = new lingkaran();

                System.out.println("Masukan Jari-jari : ");
                Lingkaran.r = Integer.parseInt(inp.nextLine());

                Lingkaran.showHasil();
                break;
            case 4:
                segitiga Segitiga = new segitiga();

                System.out.println("===Input Untuk Luas===");
                System.out.println("Masukan Tinggi");
                Segitiga.tinggi = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Alas");
                Segitiga.alas = Integer.parseInt(inp.nextLine());

                System.out.println("===Input Keliling===");
                System.out.println("Masukan Sisi A");
                Segitiga.a = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Sisi B");
                Segitiga.b = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Sisi C");
                Segitiga.c = Integer.parseInt(inp.nextLine());

                Segitiga.showHasil();
                break;
            default:
                System.out.println("Pilihan tidak ada!");
        }
    }

    // ___ Menghitung Bangun Ruang ___
    static void hitungR(int choose){
        switch (choose){
            case 1: // Kubus
                kubus Kubus = new kubus();

                System.out.println("Masukan Sisi : ");
                Kubus.sisi = Integer.parseInt(inp.nextLine());

                Kubus.showHasil();
                break;
            case 2: // Balok
                balok Balok = new balok();

                System.out.println("Masukan Tinggi : ");
                Balok.tinggi = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Panjang : ");
                Balok.panjang = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Lebar : ");
                Balok.lebar = Integer.parseInt(inp.nextLine());

                Balok.showHasil();
                break;
            case 3: // Tabung
                tabung Tabung = new tabung();

                System.out.println("Masukan Jari-jari : ");
                Tabung.jari = Integer.parseInt(inp.nextLine());
                System.out.println("Masukan Tinggi : ");
                Tabung.tinggi = Integer.parseInt(inp.nextLine());

                Tabung.showHasil();
                break;
            default:
                System.out.println("Pilihan tidak ada!");
        }

    }









}
